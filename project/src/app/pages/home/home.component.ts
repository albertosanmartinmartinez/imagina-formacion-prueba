import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/services/rest.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public elements: any;

  constructor(
    private restService: RestService
  ) {

  }

  ngOnInit(): void {
    this.getElements();
  }

  getElements() {
    
    this.restService.getColors().subscribe(result => {
      console.log(result);
      this.elements = result.data;
    });
  }

}
