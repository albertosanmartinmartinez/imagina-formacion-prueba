import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';

import { HomeRoutingModule } from './home-routing.module';
import { GuardService } from 'src/app/services/guard.service';
import { HttpClientModule } from '@angular/common/http';
import { RestService } from 'src/app/services/rest.service';



@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    HttpClientModule
  ],
  providers: [
    GuardService,
    RestService
  ]
})
export class HomeModule { }
