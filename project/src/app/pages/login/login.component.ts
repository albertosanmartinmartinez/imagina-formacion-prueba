
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { RestService } from 'src/app/services/rest.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form: FormGroup;

  constructor(
    private router: Router,
    private restService: RestService
  ) {

    this.form = this.formCreate();
    this.formOnChange();
  }

  ngOnInit(): void {
  }

  formCreate() {
    
    return new FormGroup({
      email: new FormControl('', [Validators.required, Validators.minLength(5), customEmailValidator()]),
      password: new FormControl('', [Validators.required, Validators.minLength(8), customPasswordValidator()]),
    });
  }

  formOnSubmit() {

    if (this.form.valid) {
      
      var logged: boolean = this.restService.login(this.form.value);
      console.log(logged);

      if (logged) {
        this.router.navigateByUrl('/home');
      }
      else {
        console.log('set form errors');
        this.form.get('password')?.setErrors({ badCredentials: true});
      }

      console.log(this.form.get('password')?.errors);
    }
    return;
  }

  formOnChange() {

    this.form.get('email')?.valueChanges.subscribe(change => {
      console.log(change);
      //console.log(this.form.get('email')?.errors);
    });

    this.form.get('password')?.valueChanges.subscribe(change => {
      console.log(change);
      //console.log(this.form.get('password')?.errors);
    });
  }

}

export function customPasswordValidator(): ValidatorFn {
  return (control:AbstractControl): ValidationErrors | null => {

    const value = control.value;

    const hasUpper = /[A-Z]+/.test(value);
    const hasLower = /[a-z]+/.test(value);
    const hasNumber = /[0-9]+/.test(value);
    const hasSpecial = /[&.\-_]+/.test(value);

    const passwordIsValid = hasUpper && hasLower && hasNumber && hasSpecial;

    return !passwordIsValid ? { customPasswordValidator: { hasUpper:hasUpper, hasLower:hasLower, hasNumber:hasNumber, hasSpecial:hasSpecial }}: null;
  }
}

export function customEmailValidator(): ValidatorFn {
  return (control:AbstractControl): ValidationErrors | null => {

    const value = control.value;

    const hasAt = /[@]+/.test(value);
    const hasDot = /[.]+/.test(value);

    const emailIsValid = hasAt && hasDot;

    return !emailIsValid ? { customEmailValidator: { hasAt:hasAt, hasDot:hasDot }}: null;
  }
}
