import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GuardService } from './services/guard.service';
//import { HomeComponent } from './pages/home/home.component';
//import { LoginComponent } from './pages/login/login.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    GuardService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
