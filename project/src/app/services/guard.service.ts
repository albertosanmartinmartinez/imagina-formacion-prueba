import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { RestService } from './rest.service';


@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {

  constructor(
    private router: Router,
    private restService: RestService
  ) {

  }

  canActivate(): boolean {
    console.log('canActivate function');

    if (!this.restService.logged()) {
      this.router.navigateByUrl('/login');
    }

    return true;
  }
}
