import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RestService {

  public api_url: string = 'https://reqres.in/api/login';

  constructor(
    public httpClient: HttpClient
  ) {
    
  }

  getColors(): Observable<any> {

    return this.httpClient.get<any>(this.api_url);
  }
  

  login(formValue: any): boolean {
    console.log('login function');

    if (formValue.email === 'admin@mail.com' && formValue.password === 'Abcd.1234') {
      localStorage.setItem('token', '1234');
      return true
    }

    return false;
  }

  logged(): boolean {
    console.log('logged function');
    
    const token = localStorage.getItem('token');
    if (token) {
      return true;
    }

    return false;
  }
}
